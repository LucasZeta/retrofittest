package com.lucaszeta.retrofittest;

import android.util.Log;

import com.lucaszeta.retrofittest.data.api.RetrofitTestApi;
import com.lucaszeta.retrofittest.data.model.Country;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CountryService {

    public void getAll(Callback<List<Country>> callback) {
        RetrofitTestApi api = RetrofitSingleton.getInstance().getApi();

        Call<List<Country>> call = api.listAllCountries();
        call.enqueue(callback);
    }
}
