package com.lucaszeta.retrofittest.data.api;

import com.lucaszeta.retrofittest.data.model.Country;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitTestApi {

    @GET("all")
    Call<List<Country>> listAllCountries();
}
