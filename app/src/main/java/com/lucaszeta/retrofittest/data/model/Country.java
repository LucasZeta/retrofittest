package com.lucaszeta.retrofittest.data.model;

public class Country {

    private String name;
    private String capital;
    private String flag;

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public String getFlag() {
        return flag;
    }
}
